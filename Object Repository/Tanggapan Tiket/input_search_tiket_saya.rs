<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_search_tiket_saya</name>
   <tag></tag>
   <elementGuidId>d2a7657b-be70-4191-afd3-d46ddcc161e9</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>input.form-control.form-control-sm</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@type='search']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>internal:role=searchbox[name=&quot;Search:&quot;i]</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>4bdb0f75-e68f-425c-bcc8-706ef94fa6ac</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>search</value>
      <webElementGuid>d85b9dd5-8dbc-4149-b8bf-8a74b831c8a5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control form-control-sm</value>
      <webElementGuid>163be28e-326f-4f01-93ff-c62ede3b0356</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-controls</name>
      <type>Main</type>
      <value>bootstrap-data-table</value>
      <webElementGuid>c1681c75-a0a3-49f9-8276-ca974cfe263b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;bootstrap-data-table_filter&quot;)/label[1]/input[@class=&quot;form-control form-control-sm&quot;]</value>
      <webElementGuid>5aa031a9-58c2-4d96-9356-e7d119dc3dd3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@type='search']</value>
      <webElementGuid>9275ca44-6245-4d24-94f5-fcd07de906a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='bootstrap-data-table_filter']/label/input</value>
      <webElementGuid>405333c3-9e1b-4f38-9636-82e6e0813e04</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>a38eaf09-ea1d-4865-b2e4-181ece05fbd9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'search']</value>
      <webElementGuid>5e6ded81-2bce-452f-95be-36c3561a0947</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
