<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_ojk</name>
   <tag></tag>
   <elementGuidId>2b031433-f47c-416c-8c99-770121a35f39</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-menu']/ul/li[2]/ul/li[2]/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>46fb53ec-f6a8-4b48-8128-0d00b87738d0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://10.61.4.59:7789/Tiket/form_ojk</value>
      <webElementGuid>6c90f13a-6cf6-4223-95fc-0c65c24e8d7a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>OJK</value>
      <webElementGuid>50c215e3-8202-4d42-806b-c56bb174e357</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-menu&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;menu-item-has-children dropdown show open&quot;]/ul[@class=&quot;sub-menu children dropdown-menu show&quot;]/li[2]/a[1]</value>
      <webElementGuid>cc2b6bb0-ba7f-4510-a72f-151d45d81a95</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-menu']/ul/li[2]/ul/li[2]/a</value>
      <webElementGuid>daac8a18-ec16-4ad5-ac09-450559d582b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'OJK')]</value>
      <webElementGuid>d9e821eb-4b68-4604-a732-95f70cd29612</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::a[2]</value>
      <webElementGuid>28b7834b-4fe2-4b22-b4b1-e6561c888c82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lapor'])[1]/preceding::a[1]</value>
      <webElementGuid>75776c13-cfaf-4def-85fd-ea4c0748131b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Media Sosial'])[1]/preceding::a[2]</value>
      <webElementGuid>65de7e83-1b33-4ed8-a880-c35e9b9f13e4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='OJK']/parent::*</value>
      <webElementGuid>0a08102e-41fe-427c-b2c6-73ff61325c1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://10.61.4.59:7789/Tiket/form_ojk']</value>
      <webElementGuid>5b7cf31b-2bbe-43d0-a572-2d7a9c4ff55a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/ul/li[2]/a</value>
      <webElementGuid>a0ef067f-74ae-4f9e-b786-94dd0b4a3cf3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://10.61.4.59:7789/Tiket/form_ojk' and (text() = 'OJK' or . = 'OJK')]</value>
      <webElementGuid>0bac4f60-7f0a-4d6f-9d29-1d71f0a24c28</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
