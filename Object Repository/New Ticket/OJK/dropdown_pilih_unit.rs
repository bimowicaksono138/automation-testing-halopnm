<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_pilih_unit</name>
   <tag></tag>
   <elementGuidId>c3427ba7-7ba8-4862-9dc4-e5f8c48fa499</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='op_unit']/div/div/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#op_unit > div.form-group > div.chosen-container.chosen-container-single > a.chosen-single.chosen-default > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>00f5dee6-a3a0-4348-a8b0-e5c299fe5713</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih Unit -</value>
      <webElementGuid>5df0a0bb-0370-45d0-bda0-cb66151a869d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;op_unit&quot;)/div[@class=&quot;form-group&quot;]/div[@class=&quot;chosen-container chosen-container-single&quot;]/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>614725ef-ad53-49e7-9305-b1be6c8ab8cd</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='op_unit']/div/div/a/span</value>
      <webElementGuid>209db197-692f-4b1a-8cde-b27baa4c8649</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SERANG'])[2]/following::span[1]</value>
      <webElementGuid>62d755db-aae7-4af8-9657-911a2e5fa2ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='JAKARTA'])[2]/following::span[1]</value>
      <webElementGuid>31c6cba0-f928-42c6-b4b0-20341816e88a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk Jasa Keuangan'])[1]/preceding::span[1]</value>
      <webElementGuid>afdc7627-a537-4283-8a3d-7549d1a819cf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih -'])[10]/preceding::span[1]</value>
      <webElementGuid>21c4ad90-3829-449a-9872-dca6e43d2bb3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='- Pilih Unit -']/parent::*</value>
      <webElementGuid>dd7a5d48-b0f9-4ff0-8cb4-73b1d4d94a8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/div/div/a/span</value>
      <webElementGuid>19ae8710-cd9f-441b-82a2-c1bc1b0e97c2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih Unit -' or . = '- Pilih Unit -')]</value>
      <webElementGuid>9c0223d5-1db9-4539-b6ab-138bce82b17c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
