<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_non_nasabah</name>
   <tag></tag>
   <elementGuidId>8787a70c-4783-4e6d-bbed-5e0c6afccec4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Non_Nasabah_Select_Business']/div/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>726809c1-68ee-4099-94de-5209c891268b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>modal-content</value>
      <webElementGuid>5a3b8184-1bff-458e-91ef-c7f26931d8e5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                
                    
                    Non - Nasabah
                
                
                    
                        
                            
                                 UL@MM
                                 Mekaar
                            
                        
                    
                
            </value>
      <webElementGuid>5201a8f9-7568-4b9e-8761-1a10df931cf9</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Non_Nasabah_Select_Business&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]</value>
      <webElementGuid>45e9171d-e9bc-45ea-b582-f27eb891f779</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Non_Nasabah_Select_Business']/div/div</value>
      <webElementGuid>e0e2aa7b-417a-4ac2-b13b-20a4d43a6536</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mekaar'])[2]/following::div[3]</value>
      <webElementGuid>768e3d48-607e-4c52-9546-8ff95ce9802d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='UL@MM'])[1]/following::div[3]</value>
      <webElementGuid>0f2c4e81-c317-46cc-952a-eadc36c16934</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div[3]/div/div</value>
      <webElementGuid>91a41334-6bd2-4f3f-adb0-02dd70b60e6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                
                    
                    Non - Nasabah
                
                
                    
                        
                            
                                 UL@MM
                                 Mekaar
                            
                        
                    
                
            ' or . = '
                
                    
                    Non - Nasabah
                
                
                    
                        
                            
                                 UL@MM
                                 Mekaar
                            
                        
                    
                
            ')]</value>
      <webElementGuid>944b846e-7475-467d-9fc2-cb0eb63545ab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
