<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_kategori</name>
   <tag></tag>
   <elementGuidId>ce1e2337-7750-4516-b11f-adaba60254a6</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[2]/div/div/div/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.chosen-single > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih -&quot;i >> nth=0</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>75c4cbe3-2f67-46d2-a97c-ca0a6e528b90</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih - </value>
      <webElementGuid>fa562c80-da8b-4887-893d-6637761d0649</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;right-panel&quot;)/div[@class=&quot;content&quot;]/div[@class=&quot;animated fadeIn&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body card-block&quot;]/form[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-6&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;chosen-container chosen-container-single chosen-container-active chosen-with-drop&quot;]/a[@class=&quot;chosen-single&quot;]/span[1]</value>
      <webElementGuid>96a34116-b305-4e68-afb3-a247c3a1869b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[2]/div/div/div/a/span</value>
      <webElementGuid>67fc3662-ec6a-431e-87af-b03e9626746a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kategori'])[1]/following::span[1]</value>
      <webElementGuid>c72b9d9a-700c-4313-ad4c-2c0d8ef17c37</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tanggal'])[1]/following::span[1]</value>
      <webElementGuid>8aece2bc-982a-418e-8bee-1a283d206b3c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih -'])[3]/preceding::span[1]</value>
      <webElementGuid>83217fdc-bd33-4ed4-a24b-dac63abab526</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Nasabah'])[2]/preceding::span[1]</value>
      <webElementGuid>714eeb05-5d87-4844-a004-818cf25f356d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/a/span</value>
      <webElementGuid>d0e166f6-6188-47f9-8e50-37940e8d7016</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih - ' or . = '- Pilih - ')]</value>
      <webElementGuid>27d148f2-d0f6-4c47-becb-ad41e27bcdc7</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
