<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_pic</name>
   <tag></tag>
   <elementGuidId>10ddbbae-2935-4303-aa59-dd8f3b86bc6e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[12]/div/div/div/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.chosen-container.chosen-container-single.chosen-container-active.chosen-with-drop > a.chosen-single.chosen-default > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih -&quot;i >> nth=4</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>357b1def-6906-4552-a0e7-40031d68bbd2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih -</value>
      <webElementGuid>5c0f3f9d-5894-4bfb-b3f7-9492b572bc11</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;right-panel&quot;)/div[@class=&quot;content&quot;]/div[@class=&quot;animated fadeIn&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body card-block&quot;]/form[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;chosen-container chosen-container-single chosen-container-active chosen-with-drop&quot;]/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>96a0cf09-d8fb-477d-9707-ae69efbbef05</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[12]/div/div/div/a/span</value>
      <webElementGuid>04cf2bee-9673-4f75-b8a0-6ade1cc8e437</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pic'])[1]/following::span[1]</value>
      <webElementGuid>4ff6acac-6f7f-41c7-ae18-25512c9bce18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukan Nama Kelompok'])[1]/following::span[1]</value>
      <webElementGuid>e32c922e-8640-479e-bad2-ce1e9dac7498</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dewi Andriani - Cabang Padang'])[2]/preceding::span[1]</value>
      <webElementGuid>a100a25e-86c5-418b-ac87-22632bac8335</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Eka Roestari - Divisi Pengendalian Mutu, Pengembangan Kualitas dan Audit TI - Bagian Admin SPI'])[2]/preceding::span[1]</value>
      <webElementGuid>6b79cd29-540f-4d2f-aec9-1fd80f952983</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[12]/div/div/div/a/span</value>
      <webElementGuid>149ae011-907a-4b78-af14-85cdc2994a01</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih -' or . = '- Pilih -')]</value>
      <webElementGuid>b0ec9243-9ac3-466d-b254-b8dc1b846e83</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
