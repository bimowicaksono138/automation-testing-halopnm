<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_unit</name>
   <tag></tag>
   <elementGuidId>4c9e7152-2462-48ce-b9a4-0c82b8b0a567</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='op_unit']/div/div/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.chosen-container.chosen-container-single.chosen-container-active.chosen-with-drop > a.chosen-single.chosen-default > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih Unit -&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>9211270b-5558-4528-a02a-de5a5c4eac73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih Unit -</value>
      <webElementGuid>0fd739c1-2805-40bc-9b48-093450cff061</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;op_unit&quot;)/div[@class=&quot;form-group&quot;]/div[@class=&quot;chosen-container chosen-container-single chosen-container-active chosen-with-drop&quot;]/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>7ccac110-b21f-4db0-8b4c-ac7b2002b65a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='op_unit']/div/div/a/span</value>
      <webElementGuid>5e9abac2-d214-4a4f-b0fa-8830e619f403</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Kota'])[2]/following::span[1]</value>
      <webElementGuid>20dca2a5-4da1-4d82-85d3-c1ed4f32ad46</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Cabang -'])[3]/following::span[1]</value>
      <webElementGuid>48b8f43c-f4b3-4a94-8682-774b72f1d390</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Unit -'])[3]/preceding::span[1]</value>
      <webElementGuid>80d684c8-a8b1-4129-aa33-4a0ebb0a8fa6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Kota'])[3]/preceding::span[1]</value>
      <webElementGuid>c164c01a-a5ec-4697-b523-d6a96b3d48fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div[2]/div/div/a/span</value>
      <webElementGuid>8f21fd57-9c80-4585-ab3a-1cf3fbe10ab2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih Unit -' or . = '- Pilih Unit -')]</value>
      <webElementGuid>b8575acb-e37c-400d-8d0a-e3b51cb76776</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
