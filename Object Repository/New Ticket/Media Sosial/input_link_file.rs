<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_link_file</name>
   <tag></tag>
   <elementGuidId>df352ab9-d149-4da4-925e-4e9f51867ff5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='file']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#file</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#file</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>9dbc7904-5380-491a-853d-c95fc607f250</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>file</value>
      <webElementGuid>03c3a67c-ea19-41e3-9b77-778e342d1e9f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>file</value>
      <webElementGuid>4ef40737-8266-4a20-af45-cebcf173f9bb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>5f1ffb60-cce1-4a31-8c70-d9d17ab989b0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control</value>
      <webElementGuid>3be24e1e-df04-444d-b9aa-de4c6fd8d892</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;file&quot;)</value>
      <webElementGuid>9ac8c324-a723-4c5d-887e-041a69e10d67</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='file']</value>
      <webElementGuid>61814eaf-7a68-45cb-9d1a-f705f9c4fcd6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[17]/div/div/input</value>
      <webElementGuid>d27ea790-0233-45c2-b1ac-827941b8d220</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[17]/div/div/input</value>
      <webElementGuid>3992912c-9203-4bf7-bae7-4c5daa84ea3f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'file' and @name = 'file' and @type = 'text']</value>
      <webElementGuid>2ec35298-86a9-421b-9cc8-6022b8e2f17d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
