<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_kabupaten_kota</name>
   <tag></tag>
   <elementGuidId>0c1e276d-172e-4032-94d3-d05ca725e0a1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='kota_id_chosen']/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#kota_id_chosen > a.chosen-single.chosen-default > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih Kabupaten/Kota -&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>1878b7ad-5cc9-462c-9bdf-de061d20138d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih Kabupaten/Kota -</value>
      <webElementGuid>47a222f3-f526-4d29-9b3e-157ac5026a5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;kota_id_chosen&quot;)/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>d1484f47-18d0-4ea9-b1e4-8c89d8ecc26a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='kota_id_chosen']/a/span</value>
      <webElementGuid>f6fb5183-b0c6-401b-8159-4a72ca18cdc4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Provinsi'])[1]/following::span[1]</value>
      <webElementGuid>e7ba6fb2-cfc9-4e60-886d-4e1ea1f05b9c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PAPUA BARAT'])[2]/following::span[1]</value>
      <webElementGuid>af148caf-9271-412b-bc9f-e6f0b0baad9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Kabupaten/Kota -'])[3]/preceding::span[1]</value>
      <webElementGuid>95d4d4d2-a0d1-4659-8f90-c73403daccd8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Kota'])[1]/preceding::span[1]</value>
      <webElementGuid>4e6a08a9-afa9-4b6e-8fa4-73d02f8d998a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div[2]/div/div/a/span</value>
      <webElementGuid>020c788f-2737-494f-bc61-23825c74724f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih Kabupaten/Kota -' or . = '- Pilih Kabupaten/Kota -')]</value>
      <webElementGuid>ab763585-0e22-4a05-88fb-882a557719ab</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
