<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_dropdown_unit</name>
   <tag></tag>
   <elementGuidId>059e7793-8744-4875-96ab-bd0599a53418</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(//input[@type='text'])[11]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.chosen-container.chosen-container-single.chosen-with-drop.chosen-container-active > div.chosen-drop > div.chosen-search > input.chosen-search-input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#op_unit >> internal:role=textbox</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>1920c413-59c3-4329-b21b-d462bbca6788</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>chosen-search-input</value>
      <webElementGuid>206fe557-955b-47f7-a725-94b4372d4d17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>dc536a74-0d04-44b4-a919-cbda95e90f0b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>a5d92107-cefa-4c9a-8b16-988456440612</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>bd8ffb3a-1a32-4885-ab05-ec4402208cff</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;op_unit&quot;)/div[@class=&quot;form-group&quot;]/div[@class=&quot;chosen-container chosen-container-single chosen-with-drop chosen-container-active&quot;]/div[@class=&quot;chosen-drop&quot;]/div[@class=&quot;chosen-search&quot;]/input[@class=&quot;chosen-search-input&quot;]</value>
      <webElementGuid>11c0ea52-33cf-4af5-821d-52f0f6a8d231</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[11]</value>
      <webElementGuid>d9633019-af87-4f4a-a13d-e28982b936db</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='op_unit']/div/div/div/div/input</value>
      <webElementGuid>7de6452c-9535-4508-804c-9fe2fddd1a1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[8]/div[2]/div/div/div/div/input</value>
      <webElementGuid>3daf34bf-d4d5-4f78-be9e-9093fa061075</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>561cf940-dd53-4bc9-849e-f9fa2a6a03f9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
