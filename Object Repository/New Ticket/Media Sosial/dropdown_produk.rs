<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_produk</name>
   <tag></tag>
   <elementGuidId>598bcbbb-29aa-45b9-bb77-257545aeb07b</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='produk_chosen']/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#produk_chosen > a.chosen-single.chosen-default > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#produk_chosen a</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>92c270d5-7608-43c5-b0ae-091fdad7d768</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih -</value>
      <webElementGuid>0f5b2bc0-c441-4f9c-a998-2bb626ddee79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;produk_chosen&quot;)/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>24a82d71-3323-4f41-8dc2-489057b1b3c6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='produk_chosen']/a/span</value>
      <webElementGuid>6f36089d-f398-4986-831d-2c265cfe3aef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Produk'])[1]/following::span[1]</value>
      <webElementGuid>79e22e12-9133-4bf0-8953-02a34da4d76a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Jenis Pengaduan'])[1]/following::span[1]</value>
      <webElementGuid>b2180aa7-1e60-4b38-9ecc-7f8438e33c98</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih -'])[8]/preceding::span[1]</value>
      <webElementGuid>2e7a4a74-d7fb-4cdc-aaad-429a8cdb7497</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ULaMM'])[2]/preceding::span[1]</value>
      <webElementGuid>edb4b373-e130-450f-a494-5a3296a8cda9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/a/span</value>
      <webElementGuid>b06d8652-0bab-44ef-8213-d64d17c90de3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih -' or . = '- Pilih -')]</value>
      <webElementGuid>d4e23ef1-8733-4fa3-abb4-502b3d9b3587</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
