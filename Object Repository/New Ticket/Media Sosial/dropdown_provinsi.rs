<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_provinsi</name>
   <tag></tag>
   <elementGuidId>f9604d04-2b8c-435e-aa17-10d43dbd6cc4</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='area_id_chosen']/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#area_id_chosen > a.chosen-single.chosen-default > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih Provinsi -&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>48ce1c21-807f-47e1-9d9d-2cd18f154fd0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih Provinsi -</value>
      <webElementGuid>3c43aa82-4db3-4cce-8690-b3a4a484d91a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;area_id_chosen&quot;)/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>c4e9314b-0024-4a1b-9836-534b5291e3c5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='area_id_chosen']/a/span</value>
      <webElementGuid>09d8a270-c606-467c-a5e7-97e670397e4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lokasi Kejadian'])[1]/following::span[1]</value>
      <webElementGuid>08b4adc7-0beb-47b3-9ceb-086b9a8effd2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan Nomor HP yang hanya terdiri angka'])[1]/following::span[1]</value>
      <webElementGuid>d8f324cc-01f4-4ef4-8520-13966e29b136</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Provinsi'])[1]/preceding::span[1]</value>
      <webElementGuid>1cd76f3e-3c8c-413b-9122-13a90e5f3749</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Kabupaten/Kota -'])[2]/preceding::span[1]</value>
      <webElementGuid>ca2d7c8b-c6be-45bd-8e78-9d895fab792a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[7]/div/div/div/a/span</value>
      <webElementGuid>83ad344b-6b24-4cb6-8611-97c412b0b8b5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih Provinsi -' or . = '- Pilih Provinsi -')]</value>
      <webElementGuid>be198bb4-ae5f-481f-aa10-3fdbcf4fcc32</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
