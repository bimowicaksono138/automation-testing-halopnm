<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>attachment_tiket_medsos</name>
   <tag></tag>
   <elementGuidId>658a4b64-8281-4490-82e6-0396a04b0c3e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='attachment']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#attachment</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#attachment</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>63453cf2-0ff5-4d38-930d-f206cb9e8214</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>attachment</value>
      <webElementGuid>3540e2c1-b75f-466e-8358-c9bc4602d4fb</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>attachment</value>
      <webElementGuid>448a906c-3c0d-4767-97ef-7e158f9714f8</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>file</value>
      <webElementGuid>1fe42730-58d9-44d1-904e-f809d141f623</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control-file</value>
      <webElementGuid>62aef727-0a48-4adf-b345-cf8d2cee16ba</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>accept</name>
      <type>Main</type>
      <value>.doc, .docx, .jpeg, .jpg, .png, .pdf</value>
      <webElementGuid>a6872cd0-9113-403b-9fcb-6eebdc81df82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;attachment&quot;)</value>
      <webElementGuid>fc616a56-1d3b-4054-9363-7ddbc4ee6085</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='attachment']</value>
      <webElementGuid>020ce76a-7d1c-46ff-9e5b-51f0dd878fa4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[16]/div/div/input</value>
      <webElementGuid>d7e7cd7a-94cd-4491-ab8f-bdac64811de4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[16]/div/div/input</value>
      <webElementGuid>107999f6-09f7-4b51-a1ee-deb8a5958302</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@id = 'attachment' and @name = 'attachment' and @type = 'file']</value>
      <webElementGuid>aa298445-8a8c-4107-80b8-e1e0ef412433</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
