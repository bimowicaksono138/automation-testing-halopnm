<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_platform_aduan</name>
   <tag></tag>
   <elementGuidId>3a1b1239-d20d-4a4b-bc81-aeb109ccd093</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[2]/div[2]/div/div/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.chosen-single.chosen-default > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih -&quot;i >> nth=1</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>5a15162c-20a1-45cb-bae6-2342c1f27e68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih -</value>
      <webElementGuid>5200aae2-8936-409f-8d33-7acd560e2f68</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;right-panel&quot;)/div[@class=&quot;content&quot;]/div[@class=&quot;animated fadeIn&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body card-block&quot;]/form[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-6&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;chosen-container chosen-container-single&quot;]/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>b1f43ff4-991b-4945-a2d6-613a8b09572c</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[2]/div[2]/div/div/a/span</value>
      <webElementGuid>aa4d622c-6a10-455b-bc05-09e5e05312ad</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Platform Aduan'])[1]/following::span[1]</value>
      <webElementGuid>b3ff8733-d8cd-42ec-a5f6-4cfe406409c1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Kategori'])[1]/following::span[1]</value>
      <webElementGuid>182425a4-a58e-4342-8975-49d24e20b847</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='X'])[2]/preceding::span[1]</value>
      <webElementGuid>c4fd9d93-603b-4f7d-9991-2b4930dee818</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='INSTAGRAM'])[2]/preceding::span[1]</value>
      <webElementGuid>9d53acbe-e861-41f9-96f9-e1a901063f8e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div/div/a/span</value>
      <webElementGuid>90d2c632-2afd-4f8c-98c1-cd2747a39bf9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih -' or . = '- Pilih -')]</value>
      <webElementGuid>3d6a8ec4-592e-4c3d-bf03-95b640c5ff6b</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
