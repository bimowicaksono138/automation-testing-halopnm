<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_tema_aduan</name>
   <tag></tag>
   <elementGuidId>a9bf2158-ae8f-4e19-a1b8-87fa06f22011</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[3]/div/div/div/a/span</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.chosen-container.chosen-container-single.chosen-container-active.chosen-with-drop > a.chosen-single.chosen-default > span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih -&quot;i >> nth=2</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>ae3a191e-c007-4893-85a5-c31220e0ae57</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih -</value>
      <webElementGuid>43503b9b-0b17-40d1-9244-0782d3d50f2a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;right-panel&quot;)/div[@class=&quot;content&quot;]/div[@class=&quot;animated fadeIn&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body card-block&quot;]/form[1]/div[@class=&quot;row&quot;]/div[@class=&quot;col-12&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;chosen-container chosen-container-single chosen-container-active chosen-with-drop&quot;]/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>101e3f99-f81a-4942-82ed-df4a0b4edc93</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='right-panel']/div/div/div/div/div/div[2]/form/div[3]/div/div/div/a/span</value>
      <webElementGuid>797f882d-7381-48c1-a6fe-40a397d898d8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tema Aduan'])[1]/following::span[1]</value>
      <webElementGuid>30f81000-6bbd-4fa5-a795-15eee159fe30</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Kategori'])[2]/following::span[1]</value>
      <webElementGuid>62be1798-2487-4cb5-b3a7-ba230793fb2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluhan Angsuran (Permohonan Keringanan, Angsuran Tidak Sesuai, Dll)'])[2]/preceding::span[1]</value>
      <webElementGuid>50bb2b10-f61b-4ba2-b422-1533749e762a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Keluhan Pelunasan Perihal Jaminan'])[2]/preceding::span[1]</value>
      <webElementGuid>14a36200-decf-4c6f-9b35-e25d7e66268c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div/a/span</value>
      <webElementGuid>3a1bc5b2-df1a-4569-a1ff-11c4c75bbc7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih -' or . = '- Pilih -')]</value>
      <webElementGuid>5e82eb82-83b0-4304-ab3e-d370ad127f4f</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
