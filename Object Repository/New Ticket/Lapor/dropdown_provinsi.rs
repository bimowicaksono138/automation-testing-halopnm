<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_provinsi</name>
   <tag></tag>
   <elementGuidId>8380a367-02ed-4d1e-86b6-4e7b97564602</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#area_id_chosen > a.chosen-single.chosen-default > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='area_id_chosen']/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih Provinsi -&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>7f74335b-1037-4281-88be-be2d6ea16f3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih Provinsi -</value>
      <webElementGuid>6c0fc077-1eb9-4dca-9113-ab0b983e611a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;area_id_chosen&quot;)/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>21e6da4f-a347-4d96-8171-67f4556a114b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='area_id_chosen']/a/span</value>
      <webElementGuid>66156a8f-3ae9-4f3e-9fd8-4af4a66ed5c4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lokasi Kejadian'])[1]/following::span[1]</value>
      <webElementGuid>465fbd2b-be53-4669-990d-ee7f58cae9d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Produk'])[1]/following::span[1]</value>
      <webElementGuid>f028440c-1bf5-4c6a-80a4-7567e0c99388</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Provinsi'])[1]/preceding::span[1]</value>
      <webElementGuid>2b3c1a4c-2d19-41d2-9cf6-6736aa564887</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Kabupaten/Kota -'])[2]/preceding::span[1]</value>
      <webElementGuid>f8a4c50f-c79f-4cdf-97b3-1c9dfbce0d28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div/a/span</value>
      <webElementGuid>f66f4097-c120-4ee2-a394-7b68b463608f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih Provinsi -' or . = '- Pilih Provinsi -')]</value>
      <webElementGuid>e57f348f-fe78-4ba1-b023-f504fdc19b9c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
