<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_unit</name>
   <tag></tag>
   <elementGuidId>f545f806-11a9-4658-90b7-64899fdd561e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.chosen-container.chosen-container-single.chosen-container-active.chosen-with-drop > a.chosen-single.chosen-default > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='op_unit']/div/div/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih Unit -&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>2cf1a22e-ae68-4bac-8e8e-3650a4dfdabc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih Unit -</value>
      <webElementGuid>85e1eb33-dee1-42b5-9ba3-8375ff06fc6b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;op_unit&quot;)/div[@class=&quot;form-group&quot;]/div[@class=&quot;chosen-container chosen-container-single chosen-container-active chosen-with-drop&quot;]/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>b00bbe38-8a83-4896-b21d-faa16d7fffb9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='op_unit']/div/div/a/span</value>
      <webElementGuid>17248551-3512-48e5-83fe-58708b18d0fa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Cabang'])[1]/following::span[1]</value>
      <webElementGuid>d4b539a3-6b02-4673-bf3e-6151e07f7256</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Cabang -'])[3]/following::span[1]</value>
      <webElementGuid>f935fd7f-4b9a-4465-ae63-004469ea1037</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Unit -'])[3]/preceding::span[1]</value>
      <webElementGuid>9a6223cf-f161-45d9-842f-f7cc77bece28</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Unit'])[1]/preceding::span[1]</value>
      <webElementGuid>5f14ae18-9aa2-409d-8cb4-8549f0360b92</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div[2]/div/div/a/span</value>
      <webElementGuid>8864d66e-8940-4ab5-9a67-c19005263c79</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih Unit -' or . = '- Pilih Unit -')]</value>
      <webElementGuid>d512d2cf-f30e-45f4-89da-e49b2b0a717e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
