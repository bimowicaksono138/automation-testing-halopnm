<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_cabang</name>
   <tag></tag>
   <elementGuidId>33221cf3-c962-43c9-b149-30e8de2f5b56</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.chosen-container.chosen-container-single.chosen-container-active.chosen-with-drop > a.chosen-single.chosen-default > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='op_cabang']/div/div/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih Cabang -&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>76935ccf-6a58-4169-bef3-13bbcebffea1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih Cabang -</value>
      <webElementGuid>85539756-161e-412e-88bd-7afa1ba0affc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;op_cabang&quot;)/div[@class=&quot;form-group&quot;]/div[@class=&quot;chosen-container chosen-container-single chosen-container-active chosen-with-drop&quot;]/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>4b5f06b9-e1cb-4606-91a8-09e1d4b166e8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='op_cabang']/div/div/a/span</value>
      <webElementGuid>90fe87d2-9538-429f-b8c0-dc4de6bc2b6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Kota'])[1]/following::span[1]</value>
      <webElementGuid>a0ca45da-0987-49a1-b3db-de9011d0e1d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Kabupaten/Kota -'])[3]/following::span[1]</value>
      <webElementGuid>4292bb1b-a8c9-486a-a2ce-d1f6c27d5a5b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Cabang -'])[3]/preceding::span[1]</value>
      <webElementGuid>c867672c-6d9c-45e6-a15b-975c05d3edd5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Cabang'])[1]/preceding::span[1]</value>
      <webElementGuid>e4f6c24e-3933-412e-b1dd-e54a9eb5b42c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div/div/a/span</value>
      <webElementGuid>9e6dd7f5-31eb-43a9-9120-ce268264c922</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih Cabang -' or . = '- Pilih Cabang -')]</value>
      <webElementGuid>cd4176c7-32f0-48a5-ac82-78cc1f8fe86c</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
