<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>dropdown_kabupatenkota</name>
   <tag></tag>
   <elementGuidId>7d81fb06-0bcd-48e9-b68b-4f45de0376e8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#kota_id_chosen > a.chosen-single.chosen-default > span</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='kota_id_chosen']/a/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>a >> internal:has-text=&quot;- Pilih Kabupaten/Kota -&quot;i</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>72fc15df-6949-4bd5-8c6b-1c8b5d61ed39</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>- Pilih Kabupaten/Kota -</value>
      <webElementGuid>3cc73482-33db-46f0-b25e-a22ab975a5ce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;kota_id_chosen&quot;)/a[@class=&quot;chosen-single chosen-default&quot;]/span[1]</value>
      <webElementGuid>17e4eb65-5968-43de-b867-2ba9bee950eb</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='kota_id_chosen']/a/span</value>
      <webElementGuid>c5e085a5-32f9-4459-8e0a-82f6094e3534</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Provinsi'])[1]/following::span[1]</value>
      <webElementGuid>84c611e6-1a01-4a73-bc23-6e5f9dddde66</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='PAPUA BARAT'])[2]/following::span[1]</value>
      <webElementGuid>cf53c08d-4da3-4668-bec9-6f6bca14603f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='- Pilih Kabupaten/Kota -'])[3]/preceding::span[1]</value>
      <webElementGuid>18cb091f-334f-4c92-b421-930c27639d24</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Pilih Kota'])[1]/preceding::span[1]</value>
      <webElementGuid>bcb85463-2af9-4c5a-8a2c-254501f78302</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div/div/a/span</value>
      <webElementGuid>65390118-ca9e-4327-96dc-eb0607d6a436</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '- Pilih Kabupaten/Kota -' or . = '- Pilih Kabupaten/Kota -')]</value>
      <webElementGuid>4170fbf0-1e7b-4908-b723-8e77055ddda5</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
