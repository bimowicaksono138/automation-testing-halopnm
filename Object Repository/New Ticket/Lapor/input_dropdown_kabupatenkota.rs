<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_dropdown_kabupatenkota</name>
   <tag></tag>
   <elementGuidId>174cc6d5-66cd-4c13-8b67-895748866302</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#kota_id_chosen > div.chosen-drop > div.chosen-search > input.chosen-search-input</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//*[@id=&quot;kota_id_chosen&quot;]/div/div/input</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorCollection>
      <entry>
         <key>SMART_LOCATOR</key>
         <value>#kota_id_chosen >> internal:role=textbox</value>
      </entry>
   </smartLocatorCollection>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>f60ed7e8-cdce-4669-937d-6ac466d0e343</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>chosen-search-input</value>
      <webElementGuid>03da9bbd-1cbf-4e5b-b1f9-23410c73c129</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>360fbc98-8bda-4fa6-81e7-4b0dad4fb754</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
      <webElementGuid>2d102537-7459-4b88-8341-e1e17e39a2d6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>e4fbcd34-15d7-4c74-9e12-d1b3b9e020f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;kota_id_chosen&quot;)/div[@class=&quot;chosen-drop&quot;]/div[@class=&quot;chosen-search&quot;]/input[@class=&quot;chosen-search-input&quot;]</value>
      <webElementGuid>2d6ae9d1-c884-4876-a072-a158b777a6f8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>(//input[@type='text'])[5]</value>
      <webElementGuid>22d57731-ae7b-4f02-a78b-6fe5076822d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='kota_id_chosen']/div/div/input</value>
      <webElementGuid>12bfe308-7b65-460b-8748-fa9eb416e85a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div[2]/div/div/div/div/input</value>
      <webElementGuid>5f067ae1-8b61-48a4-85a2-1500f0347054</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text']</value>
      <webElementGuid>91813f3e-a431-43ea-a092-84ea97ff8b7e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
