<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_non_nasabah_ulamm</name>
   <tag></tag>
   <elementGuidId>36af0486-c994-420c-958b-edd3cdeb0355</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='Non_Nasabah_Select_Business']/div/div/div[2]/div/div/center/a</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>c72d879b-1874-4124-9e18-ee1b356e9683</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>http://10.61.4.59:9339/Tiket/form_umum</value>
      <webElementGuid>e61b32e7-ee77-416f-bf1d-254f975e9f54</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-lg btn-rounded btn-outline-warning mb-2 mr-2</value>
      <webElementGuid>1839f293-eb70-4af5-b8ab-45b263318ef1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> UL@MM</value>
      <webElementGuid>65040fbc-4cce-4bb7-a05d-4de56e3dec3e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;Non_Nasabah_Select_Business&quot;)/div[@class=&quot;modal-dialog&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body font-size-11&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-sm-12&quot;]/center[1]/a[@class=&quot;btn btn-lg btn-rounded btn-outline-warning mb-2 mr-2&quot;]</value>
      <webElementGuid>631de4f9-7e31-4268-af5c-2c73cf8e32f5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Non_Nasabah_Select_Business']/div/div/div[2]/div/div/center/a</value>
      <webElementGuid>a5ef033b-f80e-472d-aad7-508421f601ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>(//a[contains(text(),'UL@MM')])[2]</value>
      <webElementGuid>a3bc7f1e-2a9e-4cbf-97d8-02efe7dc56f5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Non - Nasabah'])[2]/following::a[1]</value>
      <webElementGuid>9bb91413-da5b-48b6-a320-d1a8a72f0633</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mekaar'])[2]/following::a[1]</value>
      <webElementGuid>9384b6cb-9b1a-4c44-95ea-6b5ed812e4ab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mekaar'])[3]/preceding::a[1]</value>
      <webElementGuid>7d4c1106-fd4b-4b17-9e50-be3e2f6f339b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Capture object:'])[1]/preceding::a[2]</value>
      <webElementGuid>f9687dbf-56b7-4f00-b516-07756aea4d4d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[@href='http://10.61.4.59:9339/Tiket/form_umum']</value>
      <webElementGuid>1216bfaf-f67f-4b96-a17d-1dbb98e42d10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/div/div/div[2]/div/div/center/a</value>
      <webElementGuid>9cd06633-96ce-44ce-adb8-43fc4f6b5d83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = 'http://10.61.4.59:9339/Tiket/form_umum' and (text() = ' UL@MM' or . = ' UL@MM')]</value>
      <webElementGuid>68c2fa95-f12e-4a80-b1eb-79bb2b49c7f4</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
