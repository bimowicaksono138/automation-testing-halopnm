<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>link_new_tiket</name>
   <tag></tag>
   <elementGuidId>999758ec-7dc3-41fb-b969-11afce0b2f5a</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//a[@onclick='hide_sub()']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>a.dropdown-toggle</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <smartLocatorEnabled>false</smartLocatorEnabled>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>41477fb7-57a2-47d0-b7ae-1a9a2ed9793d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>#</value>
      <webElementGuid>63128757-3b15-4bda-a424-0423724a871d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>dropdown-toggle</value>
      <webElementGuid>d8aefde8-b890-4a38-be3e-8136b8452a61</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-toggle</name>
      <type>Main</type>
      <value>dropdown</value>
      <webElementGuid>7767830c-c8c0-4e76-8b0e-e183731d05c7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-haspopup</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>4bb810a2-90ad-4059-9e8c-d12df697e9ef</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-expanded</name>
      <type>Main</type>
      <value>false</value>
      <webElementGuid>feb6c500-b0c3-4718-a54b-6fbff2380514</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>onclick</name>
      <type>Main</type>
      <value>hide_sub()</value>
      <webElementGuid>ec0ccc7e-d46f-40fd-b34f-fca9256a734b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> New Ticket</value>
      <webElementGuid>cd0989c5-2410-48be-8daf-064f808a8efa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-menu&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;menu-item-has-children dropdown&quot;]/a[@class=&quot;dropdown-toggle&quot;]</value>
      <webElementGuid>e27dd1d3-46bc-47e3-9c6c-5798c212441a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@onclick='hide_sub()']</value>
      <webElementGuid>bbbcc303-e730-48eb-bbeb-80b0227e7812</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-menu']/ul/li[2]/a</value>
      <webElementGuid>86617bce-83dd-4e68-84d2-d9d86649d0c0</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::a[1]</value>
      <webElementGuid>82ddffec-a9fa-4e46-8887-b25e0dd1a901</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='New Ticket']/parent::*</value>
      <webElementGuid>16327faa-a7a3-49bc-ad9f-c2dd7662827c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '#')]</value>
      <webElementGuid>9d5d7b23-7cd2-4cc1-a233-2059a36b0dbd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[2]/a</value>
      <webElementGuid>5ff767d4-1972-4b76-8ef4-13e4829fcf70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '#' and (text() = ' New Ticket' or . = ' New Ticket')]</value>
      <webElementGuid>4f71403a-6f8c-4e24-8f56-e067ad6ebeb9</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
