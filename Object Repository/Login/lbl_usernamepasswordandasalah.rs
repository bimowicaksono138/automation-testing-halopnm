<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>lbl_usernamepasswordandasalah</name>
   <tag></tag>
   <elementGuidId>a50f0faf-1ea9-4af2-8603-6b47b4ca9ce3</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Hallo Ulamm User Login'])[1]/following::span[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>span.alr1</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>cc109fcd-f772-49ad-aed0-79013718a445</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>alr1</value>
      <webElementGuid>cfe5a1df-2c5f-4736-92d7-959acaa09185</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                username/password Anda salah                </value>
      <webElementGuid>09fb2563-b069-4fe2-b000-34c14664af8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;col-md-6 col-md-offset-3&quot;]/div[@class=&quot;bx1 col-md-8 col-md-offset-2&quot;]/form[@class=&quot;frm1&quot;]/span[@class=&quot;alr1&quot;]</value>
      <webElementGuid>262431da-49a6-4379-98ce-7df37b994808</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hallo Ulamm User Login'])[1]/following::span[1]</value>
      <webElementGuid>adc304ca-4ad4-4e0f-8e35-e7d9c8a4ffef</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/preceding::span[1]</value>
      <webElementGuid>81eb4446-bfde-414e-8ffb-6155d52a885d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Password'])[1]/preceding::span[1]</value>
      <webElementGuid>8e359097-93f8-47d5-a158-3faf2b28a73d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='username/password Anda salah']/parent::*</value>
      <webElementGuid>0ccd1cee-7250-4001-95d0-beafc2bb8451</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/span</value>
      <webElementGuid>3e67305c-8a14-4288-b7fe-3d5946501522</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = '
                username/password Anda salah                ' or . = '
                username/password Anda salah                ')]</value>
      <webElementGuid>f2c40bf0-a5de-4095-8fc1-215a7d302d10</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
