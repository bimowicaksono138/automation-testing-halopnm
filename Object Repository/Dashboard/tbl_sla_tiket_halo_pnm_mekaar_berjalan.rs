<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>tbl_sla_tiket_halo_pnm_mekaar_berjalan</name>
   <tag></tag>
   <elementGuidId>bc9c147b-676e-4929-9238-094eaca0d05f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div[4]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>bbd6d25c-91b2-43e5-87b5-6ef17d6d5b38</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-12 col-md-12</value>
      <webElementGuid>a7ad052e-4a23-4997-b61b-7df6a260f0f6</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        SLA Tiket Halo PNM Mekaar Berjalan
                        
                            
                                
                                    
                                        
                                            
                                                
                                                    
                                                        #
                                                        No. Tiket
                                                        Agent
                                                        Tanggal Create
                                                        SLA Koordinator
                                                        SLA Kabag OBS
                                                        SLA Admin PMM
                                                        SLA Kabag PMM
                                                    
                                                
                                                
                                            1CC/MKR/03/01/24001Wida Dewi Lestari2024-01-03 13:36:20.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik2CC/MKR/03/01/24002Wida Dewi Lestari2024-01-03 13:56:11.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik3CC/MKR/12/12/23417Gilang Nurgianto2024-03-13 07:57:12.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik4CC/MKR/13/03/24004Wida Dewi Lestari2024-03-13 08:08:30.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik5CC/MKR/13/11/23409Gilang Nurgianto2024-03-07 14:01:01.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik
                                        
                                    
                                    
                                        
                                    
                                
                            
                        
                    
                </value>
      <webElementGuid>2018c523-ef76-4631-a9a2-7dcf78d2ae59</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-12 col-md-12&quot;]</value>
      <webElementGuid>9cd4ff04-c4e7-4310-acc7-c91cd5a60049</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div[4]/div</value>
      <webElementGuid>20c5522e-ca5c-4643-a6e2-9c49ac851a06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Petal.Width'])[3]/following::div[3]</value>
      <webElementGuid>9e3da5df-691c-4087-81a6-456a881844a7</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sepal.Width'])[3]/following::div[3]</value>
      <webElementGuid>8e9a5683-295d-403b-b5c3-60538811e0e2</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div</value>
      <webElementGuid>542f44ad-1696-46ff-8c40-eb3c3eacea6e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        SLA Tiket Halo PNM Mekaar Berjalan
                        
                            
                                
                                    
                                        
                                            
                                                
                                                    
                                                        #
                                                        No. Tiket
                                                        Agent
                                                        Tanggal Create
                                                        SLA Koordinator
                                                        SLA Kabag OBS
                                                        SLA Admin PMM
                                                        SLA Kabag PMM
                                                    
                                                
                                                
                                            1CC/MKR/03/01/24001Wida Dewi Lestari2024-01-03 13:36:20.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik2CC/MKR/03/01/24002Wida Dewi Lestari2024-01-03 13:56:11.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik3CC/MKR/12/12/23417Gilang Nurgianto2024-03-13 07:57:12.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik4CC/MKR/13/03/24004Wida Dewi Lestari2024-03-13 08:08:30.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik5CC/MKR/13/11/23409Gilang Nurgianto2024-03-07 14:01:01.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik
                                        
                                    
                                    
                                        
                                    
                                
                            
                        
                    
                ' or . = '
                    
                        SLA Tiket Halo PNM Mekaar Berjalan
                        
                            
                                
                                    
                                        
                                            
                                                
                                                    
                                                        #
                                                        No. Tiket
                                                        Agent
                                                        Tanggal Create
                                                        SLA Koordinator
                                                        SLA Kabag OBS
                                                        SLA Admin PMM
                                                        SLA Kabag PMM
                                                    
                                                
                                                
                                            1CC/MKR/03/01/24001Wida Dewi Lestari2024-01-03 13:36:20.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik2CC/MKR/03/01/24002Wida Dewi Lestari2024-01-03 13:56:11.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik3CC/MKR/12/12/23417Gilang Nurgianto2024-03-13 07:57:12.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik4CC/MKR/13/03/24004Wida Dewi Lestari2024-03-13 08:08:30.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik5CC/MKR/13/11/23409Gilang Nurgianto2024-03-07 14:01:01.0000 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik0 Hari /  0 Jam 0 Menit 0 Detik
                                        
                                    
                                    
                                        
                                    
                                
                            
                        
                    
                ')]</value>
      <webElementGuid>da9b0472-ecdc-4371-aa88-2e25776fa3ee</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
