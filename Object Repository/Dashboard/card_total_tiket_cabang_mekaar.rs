<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_total_tiket_cabang_mekaar</name>
   <tag></tag>
   <elementGuidId>0a279674-8b06-4533-abfa-6a4598af8a87</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div[2]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>d4fc1e0b-5400-44bc-87c7-3136625da156</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-4 col-md-4</value>
      <webElementGuid>3894e6d2-3d47-457c-a3ec-d0060dd36bbf</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        Total Tiket Cabang Mekaar
                        
                            
                                
                                    
                                        
                                        
                                            
                                        
                                    
                                
                                
                            
                        
                    
                </value>
      <webElementGuid>d4388e1b-6033-4de4-ae64-5855da5ffcca</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-4 col-md-4&quot;]</value>
      <webElementGuid>fee0f9d4-4c01-4a98-8df8-4081455074b9</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div[2]/div[2]</value>
      <webElementGuid>c742b0fc-a108-497b-8313-79934ea2f825</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ULaMM'])[1]/following::div[1]</value>
      <webElementGuid>3e6cc1a9-0098-487e-9585-b3cf685d7c63</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Mekaar'])[1]/following::div[1]</value>
      <webElementGuid>9ddd4240-5767-4845-9727-0da0b8fcda02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Tiket Cabang ULaMM'])[1]/preceding::div[9]</value>
      <webElementGuid>3b2fdeff-f38e-4068-a0da-991841c2c008</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div/div/div[2]/div[2]</value>
      <webElementGuid>5816aa88-84cf-402d-a497-d778901e983a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        Total Tiket Cabang Mekaar
                        
                            
                                
                                    
                                        
                                        
                                            
                                        
                                    
                                
                                
                            
                        
                    
                ' or . = '
                    
                        Total Tiket Cabang Mekaar
                        
                            
                                
                                    
                                        
                                        
                                            
                                        
                                    
                                
                                
                            
                        
                    
                ')]</value>
      <webElementGuid>67610d72-077c-4b77-9e81-50d5103cd88a</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
