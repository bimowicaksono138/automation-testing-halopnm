<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_dashboard_report_time</name>
   <tag></tag>
   <elementGuidId>03327b0c-58cb-4436-827b-d2a170bdf844</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.card-body</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='div-filter']/div/div/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>af620de6-6f3d-4690-bb90-840130dd82f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>card-body</value>
      <webElementGuid>5a0d5989-1940-411e-bc10-b59d8eae0c1d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                            
                                
                                    Type
                                
                                
                                    
                                        Hari
                                        Bulan
                                        Tahun
                                    
                                
                                
                                    
                                    
                                        
                                            Tanggal Start
                                        
                                        
                                            
                                        
                                        
                                            Tanggal End
                                        
                                        
                                            
                                        
                                    
                                    
                                    
                                        
                                            Bulan
                                        
                                        
                                            
                                                -
                                                Januari
                                                Februari
                                                Maret
                                                April
                                                Mei
                                                Juni
                                                Juli
                                                Agustus
                                                September
                                                Oktober
                                                November
                                                Desember
                                            
                                        
                                        
                                            Tahun
                                        
                                        
                                            
                                                -
                                                2020
                                                2021
                                                2022
                                                2023
                                                2024
                                                2025
                                                2026
                                                2027
                                                2028
                                                2029
                                                2030
                                                2031
                                                2032
                                                2033
                                                2034
                                                2035
                                                2036
                                                2037
                                                2038
                                                2039
                                                2040
                                                2041
                                                2042
                                                2043
                                                2044
                                                2045
                                                2046
                                                2047
                                                2048
                                                2049
                                                2050
                                            
                                        
                                    
                                    
                                    
                                        
                                            Tahun
                                        
                                        
                                            
                                                2024
                                                2020
                                                2021
                                                2022
                                                2023
                                                2024
                                                2025
                                                2026
                                                2027
                                                2028
                                                2029
                                                2030
                                                2031
                                                2032
                                                2033
                                                2034
                                                2035
                                                2036
                                                2037
                                                2038
                                                2039
                                                2040
                                                2041
                                                2042
                                                2043
                                                2044
                                                2045
                                                2046
                                                2047
                                                2048
                                                2049
                                                2050
                                            
                                        
                                    
                                
                                
                                    
                                    
                                    
                                        
                                        
                                        
                                        
                                        
                                        
                                    
                                    
                                    
                                    
                                        13/3/2024  15:12:34
                                    

                                
                            
                        </value>
      <webElementGuid>1c70e486-4e16-4d26-8472-473cb8facd9a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;div-filter&quot;)/div[@class=&quot;col-lg-12 col-md-12&quot;]/div[@class=&quot;card&quot;]/div[@class=&quot;card-body&quot;]</value>
      <webElementGuid>dc1ab54b-9a05-4282-bf8c-b823b800e8e6</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='div-filter']/div/div/div</value>
      <webElementGuid>126889e3-d9a0-4dbd-9b44-a4d29c326547</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/following::div[7]</value>
      <webElementGuid>37f72a3c-1a3b-4c6e-a621-bd065354336c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Wida Dewi Lestari'])[1]/following::div[8]</value>
      <webElementGuid>bcc083ba-447e-4f3a-91d8-ec050fb99e40</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/div/div/div/div/div/div</value>
      <webElementGuid>13238c1c-4087-414b-944b-edba75a0e10d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                            
                                
                                    Type
                                
                                
                                    
                                        Hari
                                        Bulan
                                        Tahun
                                    
                                
                                
                                    
                                    
                                        
                                            Tanggal Start
                                        
                                        
                                            
                                        
                                        
                                            Tanggal End
                                        
                                        
                                            
                                        
                                    
                                    
                                    
                                        
                                            Bulan
                                        
                                        
                                            
                                                -
                                                Januari
                                                Februari
                                                Maret
                                                April
                                                Mei
                                                Juni
                                                Juli
                                                Agustus
                                                September
                                                Oktober
                                                November
                                                Desember
                                            
                                        
                                        
                                            Tahun
                                        
                                        
                                            
                                                -
                                                2020
                                                2021
                                                2022
                                                2023
                                                2024
                                                2025
                                                2026
                                                2027
                                                2028
                                                2029
                                                2030
                                                2031
                                                2032
                                                2033
                                                2034
                                                2035
                                                2036
                                                2037
                                                2038
                                                2039
                                                2040
                                                2041
                                                2042
                                                2043
                                                2044
                                                2045
                                                2046
                                                2047
                                                2048
                                                2049
                                                2050
                                            
                                        
                                    
                                    
                                    
                                        
                                            Tahun
                                        
                                        
                                            
                                                2024
                                                2020
                                                2021
                                                2022
                                                2023
                                                2024
                                                2025
                                                2026
                                                2027
                                                2028
                                                2029
                                                2030
                                                2031
                                                2032
                                                2033
                                                2034
                                                2035
                                                2036
                                                2037
                                                2038
                                                2039
                                                2040
                                                2041
                                                2042
                                                2043
                                                2044
                                                2045
                                                2046
                                                2047
                                                2048
                                                2049
                                                2050
                                            
                                        
                                    
                                
                                
                                    
                                    
                                    
                                        
                                        
                                        
                                        
                                        
                                        
                                    
                                    
                                    
                                    
                                        13/3/2024  15:12:34
                                    

                                
                            
                        ' or . = '
                            
                                
                                    Type
                                
                                
                                    
                                        Hari
                                        Bulan
                                        Tahun
                                    
                                
                                
                                    
                                    
                                        
                                            Tanggal Start
                                        
                                        
                                            
                                        
                                        
                                            Tanggal End
                                        
                                        
                                            
                                        
                                    
                                    
                                    
                                        
                                            Bulan
                                        
                                        
                                            
                                                -
                                                Januari
                                                Februari
                                                Maret
                                                April
                                                Mei
                                                Juni
                                                Juli
                                                Agustus
                                                September
                                                Oktober
                                                November
                                                Desember
                                            
                                        
                                        
                                            Tahun
                                        
                                        
                                            
                                                -
                                                2020
                                                2021
                                                2022
                                                2023
                                                2024
                                                2025
                                                2026
                                                2027
                                                2028
                                                2029
                                                2030
                                                2031
                                                2032
                                                2033
                                                2034
                                                2035
                                                2036
                                                2037
                                                2038
                                                2039
                                                2040
                                                2041
                                                2042
                                                2043
                                                2044
                                                2045
                                                2046
                                                2047
                                                2048
                                                2049
                                                2050
                                            
                                        
                                    
                                    
                                    
                                        
                                            Tahun
                                        
                                        
                                            
                                                2024
                                                2020
                                                2021
                                                2022
                                                2023
                                                2024
                                                2025
                                                2026
                                                2027
                                                2028
                                                2029
                                                2030
                                                2031
                                                2032
                                                2033
                                                2034
                                                2035
                                                2036
                                                2037
                                                2038
                                                2039
                                                2040
                                                2041
                                                2042
                                                2043
                                                2044
                                                2045
                                                2046
                                                2047
                                                2048
                                                2049
                                                2050
                                            
                                        
                                    
                                
                                
                                    
                                    
                                    
                                        
                                        
                                        
                                        
                                        
                                        
                                    
                                    
                                    
                                    
                                        13/3/2024  15:12:34
                                    

                                
                            
                        ')]</value>
      <webElementGuid>57e3f7af-1f29-47d1-95cd-9b7072463c64</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
