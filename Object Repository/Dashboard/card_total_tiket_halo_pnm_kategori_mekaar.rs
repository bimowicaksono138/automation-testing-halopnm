<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_total_tiket_halo_pnm_kategori_mekaar</name>
   <tag></tag>
   <elementGuidId>d2860b12-3907-4f78-b006-0c99496289ca</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div[3]/div[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>da4b5b0f-f4b6-44af-8c77-94993ad74626</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-4 col-md-4</value>
      <webElementGuid>acc99afc-f38a-4fa5-b6d5-f040bb7e54be</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        Total Tiket Halo PNM - [Kategori Mekaar]
                        
                            
                                
                                    
                                    
                                        
                                    
                                Sepal.Width0Petal.Width00.10.20.30.40.50.60.70.80.910 
                            
                        
                    
                </value>
      <webElementGuid>e9ea2f70-4fff-4dc2-910a-1c9198156d73</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-4 col-md-4&quot;]</value>
      <webElementGuid>fe5b1888-43d0-4d96-b7ed-2efeb70fd34a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div[3]/div[2]</value>
      <webElementGuid>bd182866-ccdd-44fc-9abc-699a6f0bcc6b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Petal.Width'])[1]/following::div[2]</value>
      <webElementGuid>4debe708-32ff-4757-a798-81165b53a949</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sepal.Width'])[1]/following::div[2]</value>
      <webElementGuid>e995b01b-a855-443e-ac67-5d2bc1679add</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div/div/div[3]/div[2]</value>
      <webElementGuid>4eea6d81-43f8-4b65-9d3a-7c2967b21244</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        Total Tiket Halo PNM - [Kategori Mekaar]
                        
                            
                                
                                    
                                    
                                        
                                    
                                Sepal.Width0Petal.Width00.10.20.30.40.50.60.70.80.910 
                            
                        
                    
                ' or . = '
                    
                        Total Tiket Halo PNM - [Kategori Mekaar]
                        
                            
                                
                                    
                                    
                                        
                                    
                                Sepal.Width0Petal.Width00.10.20.30.40.50.60.70.80.910 
                            
                        
                    
                ')]</value>
      <webElementGuid>0ccca938-31b1-49e5-b35d-1f5c6756f338</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
