<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_total_tiket_halo_pnm_agent</name>
   <tag></tag>
   <elementGuidId>5f6e9584-5c82-450e-bdea-01b7da3b6b3f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div[3]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>c675773f-899e-484a-b722-df250d3104f0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-4 col-md-4</value>
      <webElementGuid>4bed3b58-e795-4b61-9a4a-697d2d51bf7b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        Total Tiket Halo PNM - [Agent]
                        
                            
                                
                                    
                                    
                                        
                                    
                                Sepal.Width0Petal.Width00.10.20.30.40.50.60.70.80.910 
                            
                        
                    
                </value>
      <webElementGuid>1013a57f-c187-44ff-a1bd-b7ddf7e5ef02</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-4 col-md-4&quot;]</value>
      <webElementGuid>6145920b-cd5b-4eff-ac04-61674d5ca556</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div[3]/div</value>
      <webElementGuid>56d39abf-c39b-4d45-a384-0758bda4644c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Tiket Cabang ULaMM'])[1]/following::div[8]</value>
      <webElementGuid>c2b54004-3fc8-4054-aa73-6618d1f4dc02</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Tiket Cabang Mekaar'])[1]/following::div[17]</value>
      <webElementGuid>35871893-0881-48a8-bd45-8017c1ad9faa</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div/div/div[3]/div</value>
      <webElementGuid>2becc711-0250-4ae6-8e5f-20232ce2842f</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        Total Tiket Halo PNM - [Agent]
                        
                            
                                
                                    
                                    
                                        
                                    
                                Sepal.Width0Petal.Width00.10.20.30.40.50.60.70.80.910 
                            
                        
                    
                ' or . = '
                    
                        Total Tiket Halo PNM - [Agent]
                        
                            
                                
                                    
                                    
                                        
                                    
                                Sepal.Width0Petal.Width00.10.20.30.40.50.60.70.80.910 
                            
                        
                    
                ')]</value>
      <webElementGuid>504f676f-5bb8-47aa-940f-4786873dede6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
