<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_total_tiket_halo_pnm_kategori_ulamm</name>
   <tag></tag>
   <elementGuidId>82364fcd-89e1-458c-ad4c-77179457f2dc</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div[3]/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>8d2fa8b3-6a7f-4860-97ee-12b2ec7e62aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-4 col-md-4</value>
      <webElementGuid>40bab541-dd8e-45b9-a58a-b730b012560c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        Total Tiket Halo PNM - [Kategori ULaMM]
                        
                            
                                
                                    
                                    
                                        
                                    
                                Sepal.Width0Petal.Width00.10.20.30.40.50.60.70.80.910 
                            
                        
                    
                </value>
      <webElementGuid>1697173f-5dc0-4302-aa5d-44b3dee10e05</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-4 col-md-4&quot;]</value>
      <webElementGuid>f3490ba6-c146-4d29-a227-3b712b617eda</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div[3]/div[3]</value>
      <webElementGuid>74cea189-3ffb-44bc-860d-d0ac9ac42e7b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Petal.Width'])[2]/following::div[2]</value>
      <webElementGuid>23f1f544-509f-496f-ae33-07a18771fbe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sepal.Width'])[2]/following::div[2]</value>
      <webElementGuid>17317953-fc4d-47b4-ad6b-99cbad367e81</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div/div/div[3]/div[3]</value>
      <webElementGuid>b0cb6d42-7d8a-41eb-96e3-3a0752b4b3e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        Total Tiket Halo PNM - [Kategori ULaMM]
                        
                            
                                
                                    
                                    
                                        
                                    
                                Sepal.Width0Petal.Width00.10.20.30.40.50.60.70.80.910 
                            
                        
                    
                ' or . = '
                    
                        Total Tiket Halo PNM - [Kategori ULaMM]
                        
                            
                                
                                    
                                    
                                        
                                    
                                Sepal.Width0Petal.Width00.10.20.30.40.50.60.70.80.910 
                            
                        
                    
                ')]</value>
      <webElementGuid>1e62ecbe-e16f-44ec-91fd-10b21689f911</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
