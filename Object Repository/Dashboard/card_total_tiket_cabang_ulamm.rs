<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_total_tiket_cabang_ulamm</name>
   <tag></tag>
   <elementGuidId>226139a2-50ea-42cf-a73e-30115264a828</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div[2]/div[3]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>37ce780b-a8c5-416f-8c81-a97d114000b7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-4 col-md-4</value>
      <webElementGuid>b8f9d2ca-c3f0-494f-a556-fe321ad1fcf2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        Total Tiket Cabang ULaMM
                        
                            
                                
                                    
                                        
                                        
                                            
                                        
                                    
                                
                                
                            
                        
                    
                </value>
      <webElementGuid>3f885e6e-37b4-45e0-b061-5a93de2e875f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-4 col-md-4&quot;]</value>
      <webElementGuid>eabe2376-1d8d-4e1e-be0a-219b375623d5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div[2]/div[3]</value>
      <webElementGuid>3462f776-ace0-4542-9e97-fe155d685639</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Tiket Cabang Mekaar'])[1]/following::div[7]</value>
      <webElementGuid>bb120345-dfd1-4dd3-9cbb-02994ba28c70</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ULaMM'])[1]/following::div[10]</value>
      <webElementGuid>c28a5e60-34dc-4d82-94b6-10ae74931a7c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Tiket Halo PNM - [Agent]'])[1]/preceding::div[9]</value>
      <webElementGuid>238a5aa2-a9c6-47ee-b677-6cb612f0721a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div/div/div[2]/div[3]</value>
      <webElementGuid>d1a9a1e1-78c8-4de1-aa01-8bae195c19ae</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        Total Tiket Cabang ULaMM
                        
                            
                                
                                    
                                        
                                        
                                            
                                        
                                    
                                
                                
                            
                        
                    
                ' or . = '
                    
                        Total Tiket Cabang ULaMM
                        
                            
                                
                                    
                                        
                                        
                                            
                                        
                                    
                                
                                
                            
                        
                    
                ')]</value>
      <webElementGuid>6ae3b766-5350-488a-97b7-5b91ca78dc96</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
