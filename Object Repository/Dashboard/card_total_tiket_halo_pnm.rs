<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>card_total_tiket_halo_pnm</name>
   <tag></tag>
   <elementGuidId>525c26b8-4900-41ec-9249-bbba7628458f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.col-lg-4.col-md-4</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='main-content']/div[2]/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>ab94117a-fec3-4152-9cf9-ad88a25d9cce</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>col-lg-4 col-md-4</value>
      <webElementGuid>e90cca19-2c26-4db3-bc85-e65d5f145c89</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                    
                        Total Tiket Halo PNM
                        
                            
                                
                                    
                                        
                                        
                                            
                                        
                                    
                                    
                                        
                                             Mekaar
                                               
                                             ULaMM
                                        
                                    
                                
                            
                        
                    
                </value>
      <webElementGuid>87ad21e6-8397-44c1-b4d1-e04bb2c50c82</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;main-content&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;col-lg-4 col-md-4&quot;]</value>
      <webElementGuid>2b38a760-fdc6-4d36-8b9c-660833ef6d8d</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='main-content']/div[2]/div</value>
      <webElementGuid>87f6e08b-3247-4590-a8fa-28fcc1ff9b18</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tahun'])[3]/following::div[4]</value>
      <webElementGuid>54246cd3-95b6-42a1-bf6a-53aa4716cd94</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tahun'])[2]/following::div[7]</value>
      <webElementGuid>0d6c6c4d-ee44-4f10-878b-89a8c749c1a8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//body/div/div/div/div/div[2]/div</value>
      <webElementGuid>5c89b2cb-aa2b-480c-bf63-5f9e2f9938ec</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                    
                        Total Tiket Halo PNM
                        
                            
                                
                                    
                                        
                                        
                                            
                                        
                                    
                                    
                                        
                                             Mekaar
                                               
                                             ULaMM
                                        
                                    
                                
                            
                        
                    
                ' or . = '
                    
                        Total Tiket Halo PNM
                        
                            
                                
                                    
                                        
                                        
                                            
                                        
                                    
                                    
                                        
                                             Mekaar
                                               
                                             ULaMM
                                        
                                    
                                
                            
                        
                    
                ')]</value>
      <webElementGuid>7943837d-9ed8-4ef3-9d5c-d17f50d01569</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
