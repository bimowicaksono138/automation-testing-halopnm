import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('http://10.61.4.59:9339')

WebUI.verifyElementVisible(findTestObject('Login/input_username'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Login/input_password'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Login/btn_login'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Login/input_username'), username)

WebUI.setText(findTestObject('Login/input_password'), password)

WebUI.click(findTestObject('Login/btn_login'), FailureHandling.STOP_ON_FAILURE)

def get_text = WebUI.getText(findTestObject('Object Repository/Login/lbl_usernamepasswordandasalah'))

if (get_text.contains('username/password Anda salah')) {
   println('username/password Anda salah')
} else if (get_text.contains('Anda tidak punya otorisasi, hubungi IT Call Center')) {
    println('Anda tidak punya otorisasi, hubungi IT Call Center')
}

