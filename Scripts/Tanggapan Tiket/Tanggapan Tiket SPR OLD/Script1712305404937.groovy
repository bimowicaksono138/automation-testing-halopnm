import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/Login SPR - Positive Test'), [('username') : 'DNPinasthika1028', ('password') : 'DYDPLzNQP7OKgyrqH/vzgA=='], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Tanggapan Tiket/link_tiket_list'))

WebUI.click(findTestObject('Tanggapan Tiket/link_tiket_berjalan'))

WebUI.setText(findTestObject('Tanggapan Tiket/input_search_tiket_saya'), search_tiket_saya)

WebUI.click(findTestObject('Tanggapan Tiket/action_tanggapan'))

WebUI.setText(findTestObject('Tanggapan Tiket/input_tanggapan_tiket'), tanggapan_tiket)

WebUI.click(findTestObject('Tanggapan Tiket/radio_close_tanggapan_tiket'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('null'))

