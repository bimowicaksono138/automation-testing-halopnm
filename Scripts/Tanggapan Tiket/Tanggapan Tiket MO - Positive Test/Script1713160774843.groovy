import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//read table
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By

WebUI.callTestCase(findTestCase('Login/Login MO - Positive Test'), [('username') : 'NAmalina0702', ('password') : '4pEdhpSOaV4QR+MNWYNOAA=='], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Tanggapan Tiket/link_tiket_list'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Tanggapan Tiket/link_tiket_list'))

WebUI.verifyElementVisible(findTestObject('Tanggapan Tiket/link_tiket_berjalan'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Tanggapan Tiket/link_tiket_berjalan'))

WebUI.verifyElementVisible(findTestObject('Tanggapan Tiket/input_search_tiket_saya'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Tanggapan Tiket/input_search_tiket_saya'), search_tiket_saya)

WebDriver driver = DriverFactory.getWebDriver()

WebElement Table = driver.findElement(By.tagName('tbody'))

List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

def rows_count = rows_table.size()

for (int tb = 0; tb < rows_table.size(); tb++) {
    def col = rows_table.get(tb).findElements(By.tagName('td'))

    def tipe_tiket = col.get(0).getText()

    def nama = col.get(1).getText()

    println(nama)

    println(tipe_tiket)

    if (nama == search_tiket_saya) {
        if (tipe_tiket.contains('LAPOR')) {
            WebUI.verifyMatch(tipe_tiket.contains('LAPOR').toString(), 'true', true)

            println(tipe_tiket)
        } else if (tipe_tiket.contains('OJK')) {
            WebUI.verifyMatch(tipe_tiket.contains('OJK').toString(), 'true', true)

            println(tipe_tiket)
        } else if (tipe_tiket.contains('SOSMED')) {
            WebUI.verifyMatch(tipe_tiket.contains('SOSMED').toString(), 'true', true)

            println(tipe_tiket)
        } else {
            WebUI.verifyMatch('false', 'true', true)
        }
    } else if (nama != search_tiket_saya) {
        throw new Exception('data tidak sama')
        
        WebUI.verifyMatch('false', 'true', true)
    }
}

WebUI.verifyElementVisible(findTestObject('Tanggapan Tiket/action_tanggapan'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Tanggapan Tiket/action_tanggapan'))

WebDriver driver2 = DriverFactory.getWebDriver()

WebElement Table2 = driver2.findElement(By.tagName('tbody'))

List<WebElement> rows_table2 = Table2.findElements(By.tagName('tr'))

def rows_count2 = rows_table2.size()

def columns_row2 = rows_table2.get(0).findElements(By.tagName('td'))

def nomor_tiket = columns_row2.get(1).getText()

if (nomor_tiket == 'OJK') {
    WebUI.verifyMatch(nomorTiket.contains('OJK').toString(), 'true', true)

    println(nomor_tiket)
} else if (nomor_tiket == 'Lapor') {
    WebUI.verifyMatch(nomorTiket.contains('Lapor').toString(), 'true', true)

    println(nomor_tiket)
} else if (nomor_tiket == 'SOSMED') {
    WebUI.verifyMatch(nomorTiket.contains('SOSMED').toString(), 'true', true)

    println(nomor_tiket)
}

WebUI.verifyElementVisible(findTestObject('Tanggapan Tiket/input_tanggapan_tiket'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Tanggapan Tiket/attachment_tanggapan_tiket'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Tanggapan Tiket/btn_submit_tanggapan_tiket'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Tanggapan Tiket/input_tanggapan_tiket'), tanggapan_tiket)

String userDir = System.getProperty('user.dir')

WebUI.uploadFile(findTestObject('Tanggapan Tiket/attachment_tanggapan_tiket'), userDir + '\\Data Files\\Test Data Upload\\Attachment Tanggapan.pdf')

WebUI.click(findTestObject('Tanggapan Tiket/btn_submit_tanggapan_tiket'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('success_message'))

