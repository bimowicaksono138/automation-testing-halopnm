import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
//read table
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By as By

WebUI.callTestCase(findTestCase('Login/Login SPR - Positive Test'), [('username') : 'DNPinasthika1028', ('password') : 'DYDPLzNQP7OKgyrqH/vzgA=='], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Report/Tiket Selesai/link_report'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Report/Tiket Selesai/link_report'))

WebUI.verifyElementVisible(findTestObject('Report/Tiket Selesai/link_tiket_selesai'), FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('Report/Tiket Selesai/link_tiket_selesai'))

WebDriver driver = DriverFactory.getWebDriver()

WebElement Table = driver.findElement(By.tagName('tbody'))

List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

def rows_count = rows_table.size()

def expectedValue = 'Close'

for (int row = 0; row < rows_table.size(); row++) {
    def columns_row = rows_table.get(row).findElements(By.tagName('td'))

    def tiket = columns_row.get(0).getText()

    println(tiket)

    if (tiket.contains('LAPOR')) {
        WebUI.verifyMatch(tiket.contains('LAPOR').toString(), 'true', true)
    } else if (tiket.contains('OJK')) {
        WebUI.verifyMatch(tiket.contains('OJK').toString(), 'true', true)
    } else if (tiket.contains('SOSMED')) {
        WebUI.verifyMatch(tiket.contains('SOSMED').toString(), 'true', true)
    } else {
        WebUI.verifyMatch('false', 'true', true)
    }
    
    if (columns_row.get(12).getText().equalsIgnoreCase(expectedValue)) {
        def previousStatus = columns_row.get(12).getText()

        println(previousStatus)

        WebUI.verifyMatch(expectedValue, previousStatus, true)
    }
}

