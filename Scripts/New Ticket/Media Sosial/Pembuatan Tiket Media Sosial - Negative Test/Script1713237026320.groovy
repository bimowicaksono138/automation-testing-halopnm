import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/Login SPR - Positive Test'), [('username') : 'DNPinasthika1028', ('password') : 'DYDPLzNQP7OKgyrqH/vzgA=='], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('New Ticket/link_new_tiket'))

WebUI.click(findTestObject('New Ticket/link_new_tiket'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/link_media_sosial'))

WebUI.click(findTestObject('New Ticket/Media Sosial/link_media_sosial'))

WebUI.verifyElementText(findTestObject('lbl_tiket'), 'Tiket Media Sosial')

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/dropdown_kategori'))

WebUI.click(findTestObject('New Ticket/Media Sosial/dropdown_kategori'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_dropdown_kategori'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_dropdown_kategori'), kategori)

WebUI.sendKeys(findTestObject('New Ticket/Media Sosial/input_dropdown_kategori'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/dropdown_platform_aduan'))

WebUI.click(findTestObject('New Ticket/Media Sosial/dropdown_platform_aduan'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_dropdown__platform_aduan'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_dropdown__platform_aduan'), platform_aduan)

WebUI.sendKeys(findTestObject('New Ticket/Media Sosial/input_dropdown__platform_aduan'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/dropdown_tema_aduan'))

WebUI.click(findTestObject('New Ticket/Media Sosial/dropdown_tema_aduan'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_dropdown_tema_aduan'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_dropdown_tema_aduan'), tema_aduan)

WebUI.sendKeys(findTestObject('New Ticket/Media Sosial/input_dropdown_tema_aduan'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/dropdown_produk'))

WebUI.click(findTestObject('New Ticket/Media Sosial/dropdown_produk'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_dropdown__produk'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_dropdown__produk'), produk)

WebUI.sendKeys(findTestObject('New Ticket/Media Sosial/input_dropdown__produk'), Keys.chord(Keys.ENTER))

def value_produk = WebUI.getText(findTestObject('Object Repository/value_produk'))

println(value_produk)

if ((value_produk == 'Mekaar') || (value_produk == 'Mekaar Syariah')) {
    WebUI.verifyElementVisible(findTestObject('Object Repository/nama_kelompok'))

    WebUI.setText(findTestObject('Object Repository/nama_kelompok'), nama_kelompok)
}

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_nomor_ktp'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_nomor_ktp'), nomor_ktp)

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_nomor_hp'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_nomor_hp'), nomor_hp)

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_nama_pelapor'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_nama_pelapor'), nama_pelapor)

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/dropdown_provinsi'))

WebUI.click(findTestObject('New Ticket/Media Sosial/dropdown_provinsi'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_dropdown_provinsi'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_dropdown_provinsi'), provinsi)

WebUI.sendKeys(findTestObject('New Ticket/Media Sosial/input_dropdown_provinsi'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/dropdown_kabupaten_kota'))

WebUI.click(findTestObject('New Ticket/Media Sosial/dropdown_kabupaten_kota'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_dropdown_kabupaten_kota'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_dropdown_kabupaten_kota'), kabupaten_kota)

WebUI.sendKeys(findTestObject('New Ticket/Media Sosial/input_dropdown_kabupaten_kota'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/dropdown_cabang'))

WebUI.click(findTestObject('New Ticket/Media Sosial/dropdown_cabang'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_dropdown_cabang'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_dropdown_cabang'), cabang)

WebUI.sendKeys(findTestObject('New Ticket/Media Sosial/input_dropdown_cabang'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/dropdown_unit'))

WebUI.click(findTestObject('New Ticket/Media Sosial/dropdown_unit'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_dropdown_unit'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_dropdown_unit'), unit)

WebUI.sendKeys(findTestObject('New Ticket/Media Sosial/input_dropdown_unit'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/textarea_alamat'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/textarea_alamat'), alamat)

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/dropdown_pic'))

WebUI.click(findTestObject('New Ticket/Media Sosial/dropdown_pic'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_dropdown_pic'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_dropdown_pic'), pic)

WebUI.sendKeys(findTestObject('New Ticket/Media Sosial/input_dropdown_pic'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/textarea_keluhan'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/textarea_keluhan'), keluhan)

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/textarea_kesimpulan'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/textarea_kesimpulan'), kesimpulan)

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/attachment_tiket_medsos'))

String userDir = System.getProperty('user.dir')

WebUI.uploadFile(findTestObject('New Ticket/Media Sosial/attachment_tiket_medsos'), userDir + '\\Data Files\\Test Data Upload\\Attachment Tiket.pdf')

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/input_link_file'))

WebUI.setText(findTestObject('New Ticket/Media Sosial/input_link_file'), link_file)

WebUI.verifyElementVisible(findTestObject('New Ticket/Media Sosial/btn_submit_medsos'))

WebUI.click(findTestObject('New Ticket/Media Sosial/btn_submit_medsos'))

