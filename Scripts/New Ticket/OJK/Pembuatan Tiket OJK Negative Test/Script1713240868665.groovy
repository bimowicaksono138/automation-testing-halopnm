import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/Login SPR - Positive Test'), [('username') : 'DNPinasthika1028', ('password') : 'DYDPLzNQP7OKgyrqH/vzgA=='
        , ('role') : 'call center\r\n'], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('New Ticket/link_new_tiket'))

WebUI.click(findTestObject('New Ticket/link_new_tiket'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/link_ojk'))

WebUI.click(findTestObject('New Ticket/OJK/link_ojk'))

WebUI.verifyElementText(findTestObject('lbl_tiket'), 'Tiket OJK')

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_nomor_tiket_appk_ojk'))

WebUI.setText(findTestObject('New Ticket/OJK/input_nomor_tiket_appk_ojk'), nomor_tiket_appk_ojk)

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_tanggal_pengaduan'))

WebUI.setText(findTestObject('New Ticket/OJK/input_tanggal_pengaduan'), tanggal_pengaduan)

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_nama_pelapor'))

WebUI.setText(findTestObject('New Ticket/OJK/input_nama_pelapor'), nama_pelapor)

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/dropdown_kategori'))

WebUI.click(findTestObject('New Ticket/OJK/dropdown_kategori'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_dropdown_kategori'))

WebUI.setText(findTestObject('New Ticket/OJK/input_dropdown_kategori'), kategori)

WebUI.sendKeys(findTestObject('New Ticket/OJK/input_dropdown_kategori'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/dropdown_nama_perusahaan'))

WebUI.click(findTestObject('New Ticket/OJK/dropdown_nama_perusahaan'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_nama_perusahaan'))

WebUI.setText(findTestObject('New Ticket/OJK/input_nama_perusahaan'), nama_perusahaan)

WebUI.sendKeys(findTestObject('New Ticket/OJK/input_nama_perusahaan'), Keys.chord(Keys.ENTER))

def nama_perusahaan = WebUI.getText(findTestObject('Object Repository/value_perusahaan'))

println(nama_perusahaan)

if (nama_perusahaan == 'PT PNM - Ventura') {
    WebUI.verifyElementVisible(findTestObject('Object Repository/input_produk'))

    WebUI.setText(findTestObject('Object Repository/input_produk'), produk)
} else if (nama_perusahaan != 'PT PNM - Ventura') {
    WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/dropdown_produk'))

    WebUI.click(findTestObject('New Ticket/OJK/dropdown_produk'))

    WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_produk'))

    WebUI.setText(findTestObject('New Ticket/OJK/input_produk'), produk)

    WebUI.sendKeys(findTestObject('New Ticket/OJK/input_produk'), Keys.chord(Keys.ENTER))
}

WebUI.click(findTestObject('New Ticket/OJK/dropdown_pilih_provinsi'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_pilih_provinsi'))

WebUI.setText(findTestObject('New Ticket/OJK/input_pilih_provinsi'), provinsi)

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_pilih_provinsi'))

WebUI.sendKeys(findTestObject('New Ticket/OJK/input_pilih_provinsi'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/dropdown_kabupatenkota'))

WebUI.click(findTestObject('New Ticket/OJK/dropdown_kabupatenkota'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_kabupatenkota'))

WebUI.setText(findTestObject('New Ticket/OJK/input_kabupatenkota'), kabupaten_kota)

WebUI.sendKeys(findTestObject('New Ticket/OJK/input_kabupatenkota'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/dropdown_cabang'))

WebUI.click(findTestObject('New Ticket/OJK/dropdown_cabang'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_cabang'))

WebUI.setText(findTestObject('New Ticket/OJK/input_cabang'), cabang)

WebUI.sendKeys(findTestObject('New Ticket/OJK/input_cabang'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/dropdown_pilih_unit'))

WebUI.click(findTestObject('New Ticket/OJK/dropdown_pilih_unit'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_pilih_unit'))

WebUI.setText(findTestObject('New Ticket/OJK/input_pilih_unit'), unit)

WebUI.sendKeys(findTestObject('New Ticket/OJK/input_pilih_unit'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/dropdown_produk_jasa_keuangan'))

WebUI.click(findTestObject('New Ticket/OJK/dropdown_produk_jasa_keuangan'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_produk_jasa_keuangan'))

WebUI.setText(findTestObject('New Ticket/OJK/input_produk_jasa_keuangan'), produk_jasa_keuangan)

WebUI.sendKeys(findTestObject('New Ticket/OJK/input_produk_jasa_keuangan'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/dropdown_jenis_aduan'))

WebUI.click(findTestObject('New Ticket/OJK/dropdown_jenis_aduan'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_jenis_aduan'))

WebUI.setText(findTestObject('New Ticket/OJK/input_jenis_aduan'), jenis_aduan)

WebUI.sendKeys(findTestObject('New Ticket/OJK/input_jenis_aduan'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/dropdown_pic'))

WebUI.click(findTestObject('New Ticket/OJK/dropdown_pic'))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_pic'))

WebUI.setText(findTestObject('New Ticket/OJK/input_pic'), pic_cabang)

WebUI.sendKeys(findTestObject('New Ticket/OJK/input_pic'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_keluhan'))

WebUI.setText(findTestObject('New Ticket/OJK/input_keluhan'), keluhan)

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_kesimpulan'))

WebUI.setText(findTestObject('New Ticket/OJK/input_kesimpulan'), kesimpulan)

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/attachment_tiket_ojk'))

String userDir = System.getProperty('user.dir')

WebUI.uploadFile(findTestObject('New Ticket/OJK/attachment_tiket_ojk'), userDir + '\\Data Files\\Test Data Upload\\Attachment Tiket.pdf')

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/input_link_file'))

WebUI.setText(findTestObject('New Ticket/OJK/input_link_file'), link_file)

WebUI.verifyElementVisible(findTestObject('New Ticket/OJK/btn_submit_ojk'))

WebUI.click(findTestObject('New Ticket/OJK/btn_submit_ojk'))

