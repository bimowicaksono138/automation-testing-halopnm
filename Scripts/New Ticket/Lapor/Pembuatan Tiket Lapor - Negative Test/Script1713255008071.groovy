import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Login/Login SPR - Positive Test'), [('username') : 'DNPinasthika1028', ('password') : 'DYDPLzNQP7OKgyrqH/vzgA=='
        , ('role') : 'call center\r\n'], FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('New Ticket/link_new_tiket'))

WebUI.click(findTestObject('New Ticket/link_new_tiket'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/link_lapor'))

WebUI.click(findTestObject('New Ticket/Lapor/link_lapor'))

WebUI.verifyElementText(findTestObject('lbl_tiket'), 'Tiket Lapor')

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_nama_pelapor'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_nama_pelapor'), nama_pelapor)

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_nomor_ktp'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_nomor_ktp'), nomor_ktp)

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_nomor_hp'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_nomor_hp'), nomor_hp)

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/dropdown_kategori'))

WebUI.click(findTestObject('New Ticket/Lapor/dropdown_kategori'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_dropdown_kategori'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_dropdown_kategori'), kategori)

WebUI.sendKeys(findTestObject('New Ticket/Lapor/input_dropdown_kategori'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/dropdown_produk'))

WebUI.click(findTestObject('New Ticket/Lapor/dropdown_produk'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_dropdown_produk'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_dropdown_produk'), produk)

WebUI.sendKeys(findTestObject('New Ticket/Lapor/input_dropdown_produk'), Keys.chord(Keys.ENTER))

def value_produk = WebUI.getText(findTestObject('Object Repository/New Ticket/Lapor/value_produk'))

println(value_produk)

if ((value_produk == 'Mekaar') || (value_produk == 'Mekaar Syariah')) {
    WebUI.verifyElementVisible(findTestObject('Object Repository/input_kelompok'))

    WebUI.setText(findTestObject('Object Repository/input_kelompok'), nama_kelompok)
}

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/dropdown_provinsi'))

WebUI.click(findTestObject('New Ticket/Lapor/dropdown_provinsi'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_dropdown_provinsi'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_dropdown_provinsi'), provinsi)

WebUI.sendKeys(findTestObject('New Ticket/Lapor/input_dropdown_provinsi'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/dropdown_kabupatenkota'))

WebUI.click(findTestObject('New Ticket/Lapor/dropdown_kabupatenkota'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_dropdown_kabupatenkota'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_dropdown_kabupatenkota'), kabupaten_kota)

WebUI.sendKeys(findTestObject('New Ticket/Lapor/input_dropdown_kabupatenkota'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/dropdown_cabang'))

WebUI.click(findTestObject('New Ticket/Lapor/dropdown_cabang'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_dropdown_cabang'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_dropdown_cabang'), cabang)

WebUI.sendKeys(findTestObject('New Ticket/Lapor/input_dropdown_cabang'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/dropdown_unit'))

WebUI.click(findTestObject('New Ticket/Lapor/dropdown_unit'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_dropdown_unit'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_dropdown_unit'), unit)

WebUI.sendKeys(findTestObject('New Ticket/Lapor/input_dropdown_unit'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_alamat'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_alamat'), alamat)

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/dropdown_jenis_aduan'))

WebUI.click(findTestObject('New Ticket/Lapor/dropdown_jenis_aduan'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_dropdown_jenis_aduan'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_dropdown_jenis_aduan'), jenis_aduan)

WebUI.sendKeys(findTestObject('New Ticket/Lapor/input_dropdown_jenis_aduan'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/dropdown_pic'))

WebUI.click(findTestObject('New Ticket/Lapor/dropdown_pic'))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_dropdown_pic'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_dropdown_pic'), pic)

WebUI.sendKeys(findTestObject('New Ticket/Lapor/input_dropdown_pic'), Keys.chord(Keys.ENTER))

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_keluhan'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_keluhan'), keluhan)

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_kesimpulan'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_kesimpulan'), kesimpulan)

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/attachment_tiket_lapor'))

String userDir = System.getProperty('user.dir')

WebUI.uploadFile(findTestObject('New Ticket/Lapor/attachment_tiket_lapor'), userDir + '\\Data Files\\Test Data Upload\\Attachment Tiket.pdf')

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/input_link_file'))

WebUI.setText(findTestObject('New Ticket/Lapor/input_link_file'), link_file)

WebUI.verifyElementVisible(findTestObject('New Ticket/Lapor/btn_submit_lapor'))

WebUI.click(findTestObject('New Ticket/Lapor/btn_submit_lapor'))

